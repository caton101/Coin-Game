# Coin Game Copyright (C) 2018 Cameron Himes
# version 1.1

#   This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#   This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#   You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

# write intro to terminal
print("Coin Game by Code Scribe.")
print("Version 1.2")
print("---")
print("Coin Game Copyright (C) 2018 Cameron Himes")
print("This program comes with ABSOLUTELY NO WARRANTY.")
print("This is free software, and you are welcome to redistribute it under certain conditions.")
print("Start this program with -l to read the license. Alternatively, you can open the LICENSE file in a text viewer/editor.")

import random
import os
from playsound import playsound
import curses
import math

# Settings
coin_limit = 10
max_score = 2000
turn_length = 20
spawn_probability = 75
dev_mode = False

# other vars
coins_placed = 0
coins = []
players = []
sound_samples = {
    'side' : "assets/move_horizontal.wav",
    'up' : "assets/move_up.wav",
    'down' : "assets/move_down.wav",
    'boot' : "assets/event_bootup.wav",
    'score' : "assets/event_score.wav",
    'wall' : "assets/event_hit_wall.wav",
    'coin' : "assets/event_coin_placed.wav",
    'turn2' : "assets/event_end_turn_2.wav",
    'turn' : "assets/event_end_turn.wav"
}

characters = {
    'blank' : " ",
    'state0' : "+",
    'state1' : "X",
    'border' : "#",
    'coin' : "@"
}

class Player(object):
    state = 1
    name = "TESTER"
    x = 10
    y = 10
    score = 0
    turns = 10
    is_AI = True
    def __init__(self,x,y,score,state,name,turns,is_AI):
        self.x = x
        self.y = y
        self.score = score
        self.state = state
        self.name = name
        self.turns = turns
        self.is_AI = is_AI
        self.move("INIT")
    def scored(self):
        if coins_placed == 1:
            self.score += 100
            # Game needs more coins
            coin_gen(True)
        elif coins_placed == 2:
            self.score += 75
        elif coins_placed == 3:
            self.score += 50
        elif coins_placed == 4:
            self.score += 25
        else:
            self.score += 1
        if self.score >= max_score:
            end_game(self.name)
    def ai_move(self):
        # get distance to all coins
        coin_dist = []
        for coin in coins:
            # point distance = sqrt( (x1 - x2)^2 + (y1 - y2)^2 )
            coin_dist.append(math.sqrt(((self.x - coin[0]) ** 2) + ((self.y - coin[1]) ** 2)))
        #print("The distances are: %s" % (coin_dist))
        # pick closest coin
        target = coins[coin_dist.index(min(coin_dist))]
        # move to it
        if not self.x == target[0]:
            # match x axis
            if self.x > target[0]:
                self.move("LEFT")
                return
            elif self.x < target[0]:
                self.move("RIGHT")
                return
        if not self.y == target[1]:
            # match y axis
            if self.y > target[1]:
                self.move("UP")
                return
            elif self.y < target[1]:
                self.move("DOWN")
                return
    def move(self,direction):
        if direction == "UP":
            window.addch(self.y,self.x,characters['blank'])
            if self.y >= playable[0][1]+1:
                self.y -= 1
                play_sound("UP")
                self.turns -= 1
            else:
                play_sound("WALL")
            self.check()
            window.addch(self.y,self.x,characters['state0'])
        elif direction == "DOWN":
            window.addch(self.y,self.x,characters['blank'])
            if self.y <= playable[1][1]:
                self.y += 1
                play_sound("DOWN")
                self.turns -= 1
            else:
                play_sound("WALL")
            self.check()
            window.addch(self.y,self.x,characters['state0'])
        elif direction == "LEFT":
            window.addch(self.y,self.x,characters['blank'])
            if self.x >= playable[0][0]+1:
                self.x -= 1
                play_sound("SIDE")
                self.turns -= 1
            else:
                play_sound("WALL")
            self.check()
            window.addch(self.y,self.x,characters['state0'])
        elif direction == "RIGHT":
            window.addch(self.y,self.x,characters['blank'])
            if self.x <= playable[1][0]:
                self.x += 1
                play_sound("SIDE")
                self.turns -= 1
            else:
                play_sound("WALL")
            self.check()
            window.addch(self.y,self.x,characters['state0'])
        elif direction == "INIT":
            window.addch(self.y,self.x,characters['state0'])
        coin_gen(False)
        window.refresh()
    def check(self):
        global coins_placed
        if [self.x,self.y] in coins:
            self.scored()
            coins.remove([self.x,self.y])
            coins_placed -= 1
            play_sound("SCORE")
            self.turns += math.floor(turn_length / 2)
        window.refresh()

# sound engine
def play_sound(event):
    playsound(sound_samples[event.lower()])

# make routine to end game
def end_game(name):
    # close curses
    curses.endwin()
    print("\nPlayer %s has won the game!\n" % (name))
    exit()


# make coin generator
def coin_gen(forced):
    global coins_placed
    global coins
    if ((random.random() < 1/spawn_probability) | forced) and (coins_placed < coin_limit):
        target_x = random.randrange(playable[0][0],playable[1][0])
        target_y = random.randrange(playable[0][1],playable[1][1])
        window.addch(target_y,target_x,characters['coin'])
        coins.append([target_x,target_y])
        coins_placed += 1
        play_sound("COIN")
        window.refresh()

# load screen data
screen = os.get_terminal_size()
window_height = screen.lines
window_width = screen.columns

# initialize curses
window = curses.initscr()
curses.noecho()

# draw the frame
for x in range(window_width-1):
    window.addch(0,x,characters['border']) #top
    window.addch(window_height-2,x,characters['border']) #bottom
for y in range(window_height-1):
    window.addch(y,0,characters['border']) #left
    window.addch(y,window_width-2,characters['border']) #right
window.refresh()

playable = [[1,1],[window_width-4,window_height-4]] # top left (x,y) , bottom right (x,y)


# place initial coins
for x in range(5):
    coin_gen(True)

players.append(Player(10,10,0,0,"Human",10,False))
players.append(Player(20,20,0,0,"Robot",10,True))

# enter main routine
play_sound("BOOT")
while True:
    for player in range(len(players)):
        players[player].turns = turn_length
        while True:
            # notify players of turn
            window.addstr(window_height-1,0,"Turn: %s     X/Y: %s,%s     Score: %s/%s     Coins Available: %s     Moves Left: %s     " % (players[player].name,players[player].x,players[player].y,players[player].score,max_score,coins_placed,players[player].turns))
            window.refresh()
            # check if human
            if players[player].is_AI:
                # run AI brain
                players[player].ai_move()
            else:
                # do control routine
                ans = window.getch()
                if (ans == 27) or (ans == 101):
                    curses.endwin()
                    exit()
                elif ans == 119:
                    players[player].move("UP")
                elif ans == 115:
                    players[player].move("DOWN")
                elif ans == 97:
                    players[player].move("LEFT")
                elif ans == 100:
                    players[player].move("RIGHT")
                elif (ans == 112) or (ans == 113):
                    play_sound("TURN2")
                    break
                else:
                    if dev_mode:
                        window.addstr(0,0,"Unknown key identifier: %s" % (ans))
            # check turn count
            if players[player].turns <= 0:
                # notify players
                play_sound("TURN2")
                # end turn
                break
