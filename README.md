# Coin Game

This is a simple game where a player and a bot compete to collect coins

![screenshot](Screenshot.png)

## Usage

`cd src`

`python3 coin_game.py`

## Keybinds

| Key | Purpose    |
| :-: | :--------- |
| Esc | quit game  |
| q   | end turn   |
| e   | quit game  |
| p   | end turn   |
| w   | move up    |
| a   | move left  |
| s   | move down  |
| d   | move right |
